import web3
import eth_tester
import json


eth_params = eth_tester.backends.pyevm.main.get_default_genesis_params({
    'gas_limit': 9000000,
    })
backend = eth_tester.PyEVMBackend(eth_params)
instance = eth_tester.EthereumTester(backend)
provider = web3.Web3.EthereumTesterProvider(instance)
w3 = web3.Web3(provider)


f = open('AccountsIndex.bin', 'r')
bytecode = f.read()
f.close()

f = open('AccountsIndex.json', 'r')
abi = json.load(f)
f.close()

c = w3.eth.contract(abi=abi, bytecode=bytecode)
tx_hash = c.constructor().transact({'from': w3.eth.accounts[0]})

r = w3.eth.getTransactionReceipt(tx_hash)

c = w3.eth.contract(abi=abi, address=r.contractAddress)
fail = False
try:
    c.functions.add(w3.eth.accounts[2]).transact({'from': w3.eth.accounts[1]})
except:
    fail = True
assert fail

c.functions.addWriter(w3.eth.accounts[1]).transact({'from': w3.eth.accounts[0]})
c.functions.add(w3.eth.accounts[2]).transact({'from': w3.eth.accounts[1]})
c.functions.add(w3.eth.accounts[3]).transact({'from': w3.eth.accounts[1]})

assert c.functions.count().call() == 3
assert c.functions.accountsIndex(w3.eth.accounts[3]).call() == 2
assert c.functions.accounts(2).call() == w3.eth.accounts[3]

fail = False
try:
    c.functions.add(w3.eth.accounts[3]).transact({'from': w3.eth.accounts[1]})
except:
    fail = True
assert fail
