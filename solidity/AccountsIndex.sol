pragma solidity >0.6.11;

// SPDX-License-Identifier: GPL-3.0-or-later

contract CustodialAccountIndex {

	address[] entries;
	mapping(address => uint256) entryIndex;
	uint256 count;
	mapping(address => bool) writers;
	address public owner;
	address newOwner;

	event AddressAdded(address indexed addedAccount, uint256 indexed accountIndex); // AccountsIndex
	event OwnershipTransferred(address indexed previousOwner, address indexed newOwner); // EIP173

	constructor() public {
		owner = msg.sender;
		entries.push(address(0));
		count = 1;
	}

	function addWriter(address _writer) public returns (bool) {
		require(owner == msg.sender);
		writers[_writer] = true;
		return true;
	}

	function deleteWriter(address _writer) public returns (bool) {
		require(owner == msg.sender);
		delete writers[_writer];
		return true;
	}

	// Implements AccountsIndex
	function add(address _account) external returns (bool) {
		require(writers[msg.sender]);
		require(entryIndex[_account] == 0);
		entryIndex[_account] = entries.length;
		entries.push(_account);
		emit AddressAdded(_account, entries.length-1);
		return true;
	}

	// Implements AccountsIndex
	function have(address _account) external view returns (bool) {
		return entryIndex[_account] > 0;
	}

	// Implements AccountsIndex
	function entry(uint256 _idx) public returns (address) {
		return entries[_idx+1];
	}

	// Implements AccountsIndex
	function entryCount() public returns (uint256) {
		return count - 1;
	}

	// Implements EIP173
	function transferOwnership(address _newOwner) public returns (bool) {
		require(msg.sender == owner);
		newOwner = _newOwner;
	}

	// Implements OwnedAccepter
	function acceptOwnership() public returns (bool) {
		address oldOwner;

		require(msg.sender == newOwner);
		oldOwner = owner; 
		owner = newOwner;
		newOwner = address(0);
		emit OwnershipTransferred(oldOwner, owner);
	}

	// Implements EIP165
	function supportsInterface(bytes4 _sum) public pure returns (bool) {
		if (_sum == 0xcbdb05c7) { // AccountsIndex
			return true;
		}
		if (_sum == 0x01ffc9a7) { // EIP165
			return true;
		}
		if (_sum == 0x9493f8b2) { // EIP173
			return true;
		}
		if (_sum == 0x37a47be4) { // OwnedAccepter
			return true;
		}
		return false;
	}
}
