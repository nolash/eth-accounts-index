FROM ethereum/solc:0.6.12

FROM python:3.8.6-alpine

COPY --from=0 /usr/bin/solc /usr/bin/solc

RUN apk update &&\
	apk add gcc bash musl-dev

WORKDIR /usr/src

# Try to keep everything above here re-usable!

COPY ./solidity/ /usr/src/eth_accounts_index/solidity/
COPY ./python/ /usr/src/eth_accounts_index/python/

RUN cd eth_accounts_index/solidity && \
	solc AccountsIndex.sol --abi | awk 'NR>3' > AccountsIndex.abi.json

RUN cd eth_accounts_index/solidity && \
	solc AccountsIndex.sol --bin | awk 'NR>3' > AccountsIndex.bin && \
	truncate -s "$((`stat -t -c "%s" AccountsIndex.bin`-1))" AccountsIndex.bin

RUN cd eth_accounts_index/python && \
	pip install --extra-index-url https://pip.grassrootseconomics.net:8433 .

# To deploy:
# eth-accounts-index --contracts-dir eth_accounts_index/solidity/ [...]
