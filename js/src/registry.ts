class AccountRegistry {

	contract:	any

	constructor(w3:any, abi:object, address:string) {
		this.contract = new w3.eth.Contract(abi, address);
	}


	public async count(): Promise<number> {
		return this.contract.methods.count().call();
	}


	public async have(address:string): Promise<boolean> { 
		return await this.contract.methods.accountsIndex(address).call() != 0;
	}


	public async last(n:number): Promise<Array<string>> {
		const c = await this.count();
		let lo = c - n - 1;
		if (lo < 0) {
			lo = 0;
		}
		let accounts = [];
		for (let i = c - 1; i > lo; i--) {
			console.log('i', i);
			console.log('foo', i, await this.contract.methods.accounts(i).call());
			const a = await this.contract.methods.accounts(i).call();
			accounts.push(a);
		}
		return accounts;
	}
}

export {
	AccountRegistry,
}
